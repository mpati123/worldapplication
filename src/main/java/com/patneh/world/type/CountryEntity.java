package com.patneh.world.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "country", schema = "public", catalog = "world-db")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountryEntity {
    @JsonIgnore
    private String code;
    private String name;
    private String continent;
    @JsonIgnore
    private String region;
    @JsonIgnore
    private float surfaceArea;
    @JsonIgnore
    private Short indepYear;
    private int population;
    private Float lifeExpectancy;
    @JsonIgnore
    private BigDecimal gnp;
    @JsonIgnore
    private BigDecimal gnpOld;
    @JsonIgnore
    private String localName;
    @JsonIgnore
    private String governmentForm;
    @JsonIgnore
    private String headOfState;
    @JsonIgnore
    private String code2;
    @JsonIgnore
    private CityEntity cityByCapital;

    @Id
    @Column(name = "code", nullable = false, length = 3)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name", nullable = false, length = -1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "continent", nullable = false, length = -1)
    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    @Basic
    @Column(name = "region", nullable = false, length = -1)
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Basic
    @Column(name = "surface_area", nullable = false, precision = 0)
    public float getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(float surfaceArea) {
        this.surfaceArea = surfaceArea;
    }

    @Basic
    @Column(name = "indep_year", nullable = true)
    public Short getIndepYear() {
        return indepYear;
    }

    public void setIndepYear(Short indepYear) {
        this.indepYear = indepYear;
    }

    @Basic
    @Column(name = "population", nullable = false)
    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Basic
    @Column(name = "life_expectancy", nullable = true, precision = 0)
    public Float getLifeExpectancy() {
        return lifeExpectancy;
    }

    public void setLifeExpectancy(Float lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    @Basic
    @Column(name = "gnp", nullable = true, precision = 2)
    public BigDecimal getGnp() {
        return gnp;
    }

    public void setGnp(BigDecimal gnp) {
        this.gnp = gnp;
    }

    @Basic
    @Column(name = "gnp_old", nullable = true, precision = 2)
    public BigDecimal getGnpOld() {
        return gnpOld;
    }

    public void setGnpOld(BigDecimal gnpOld) {
        this.gnpOld = gnpOld;
    }

    @Basic
    @Column(name = "local_name", nullable = false, length = -1)
    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    @Basic
    @Column(name = "government_form", nullable = false, length = -1)
    public String getGovernmentForm() {
        return governmentForm;
    }

    public void setGovernmentForm(String governmentForm) {
        this.governmentForm = governmentForm;
    }

    @Basic
    @Column(name = "head_of_state", nullable = true, length = -1)
    public String getHeadOfState() {
        return headOfState;
    }

    public void setHeadOfState(String headOfState) {
        this.headOfState = headOfState;
    }

    @Basic
    @Column(name = "code2", nullable = false, length = 2)
    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryEntity that = (CountryEntity) o;
        return Float.compare(that.surfaceArea, surfaceArea) == 0 &&
                population == that.population &&
                Objects.equals(code, that.code) &&
                Objects.equals(name, that.name) &&
                Objects.equals(continent, that.continent) &&
                Objects.equals(region, that.region) &&
                Objects.equals(indepYear, that.indepYear) &&
                Objects.equals(lifeExpectancy, that.lifeExpectancy) &&
                Objects.equals(gnp, that.gnp) &&
                Objects.equals(gnpOld, that.gnpOld) &&
                Objects.equals(localName, that.localName) &&
                Objects.equals(governmentForm, that.governmentForm) &&
                Objects.equals(headOfState, that.headOfState) &&
                Objects.equals(code2, that.code2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, continent, region, surfaceArea, indepYear, population, lifeExpectancy, gnp, gnpOld, localName, governmentForm, headOfState, code2);
    }

    @ManyToOne
    @JoinColumn(name = "capital", referencedColumnName = "id")
    public CityEntity getCityByCapital() {
        return cityByCapital;
    }

    public void setCityByCapital(CityEntity cityByCapital) {
        this.cityByCapital = cityByCapital;
    }
}
