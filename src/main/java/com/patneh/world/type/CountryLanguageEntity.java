package com.patneh.world.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "country_language", schema = "public", catalog = "world-db")
@IdClass(CountryLanguageEntityPK.class)
@JsonInclude
public class CountryLanguageEntity {
    @JsonIgnore
    private String countryCode;
    private String language;
    @JsonIgnore
    private boolean isOfficial;
    @JsonIgnore
    private float percentage;
    private CountryEntity countryByCountryCode;

    @Id
    @Column(name = "country_code", nullable = false, length = 3)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Id
    @Column(name = "language", nullable = false, length = -1)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "is_official", nullable = false)
    public boolean isOfficial() {
        return isOfficial;
    }

    public void setOfficial(boolean official) {
        isOfficial = official;
    }

    @Basic
    @Column(name = "percentage", nullable = false, precision = 0)
    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryLanguageEntity that = (CountryLanguageEntity) o;
        return isOfficial == that.isOfficial &&
                Float.compare(that.percentage, percentage) == 0 &&
                Objects.equals(countryCode, that.countryCode) &&
                Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryCode, language, isOfficial, percentage);
    }

    @ManyToOne
    @JoinColumn(name = "country_code", referencedColumnName = "code", nullable = false, insertable = false, updatable = false)
    public CountryEntity getCountryByCountryCode() {
        return countryByCountryCode;
    }

    public void setCountryByCountryCode(CountryEntity countryByCountryCode) {
        this.countryByCountryCode = countryByCountryCode;
    }

    @Override
    public String toString() {
        return new StringJoiner("\n", "{\n", "\n}")
                .add("\"name\": \""+countryByCountryCode.getName() + "\"\n")
                .add("\"continent\": \"" + countryByCountryCode.getContinent() + "\"")
                .add("\"population\": \"" + countryByCountryCode.getPopulation() + "\"")
                .add("\"life_expectancy\": \"" + countryByCountryCode.getLifeExpectancy() + "\"")
                .add("\"language\": \"" + language + "\"")
                .toString();
    }
}
