package com.patneh.world.controller;

import com.patneh.world.repository.CityRepository;
import com.patneh.world.repository.CountryLanguageRepository;
import com.patneh.world.repository.CountryRepository;
import com.patneh.world.type.CountryEntity;
import com.patneh.world.type.CountryLanguageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class WorldController {

    @Autowired
    public CountryRepository countryRepository;

    @Autowired
    public CityRepository cityRepository;

    @Autowired
    public CountryLanguageRepository countryLanguageRepository;

    @GetMapping("/{countryCode}")
    public String getCountryInfo(@PathVariable String countryCode){
        if(countryLanguageRepository.existsByCountryCode(countryCode)){
            return countryLanguageRepository.findByCountryCodeAndOfficialIs(countryCode, true).toString();
        }
        return "INVALID_COUNTRY_CODE";
    }

    /*@RequestMapping(path = "/{countryCode}", method = RequestMethod.GET)
    @ResponseBody
    public String indexPage(@PathVariable("countryCode") String countryCode){
        StringBuilder response = new StringBuilder();
        CountryEntity country = countryRepository.findByCode(countryCode);
        CountryLanguageEntity language = countryLanguageRepository.findByCountryCodeAndOfficialIs(countryCode,true);
        response.append("name: ").append(country.getName())
                .append("\ncontinent: ").append(country.getContinent())
                .append("\npopulation: ").append(country.getPopulation())
                .append("\nlife_expectancy: ").append(country.getLifeExpectancy())
                .append("\ncountry_language: ").append(language.getLanguage());
        return response.toString();
    }*/
}





