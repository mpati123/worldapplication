package com.patneh.world.repository;

import com.patneh.world.type.CountryLanguageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryLanguageRepository extends JpaRepository<CountryLanguageEntity, String> {
    CountryLanguageEntity findByCountryCodeAndOfficialIs(String code, boolean isTrue);
    boolean existsByCountryCode(String code);
}
