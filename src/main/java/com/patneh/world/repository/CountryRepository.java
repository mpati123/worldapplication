package com.patneh.world.repository;

import com.patneh.world.type.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<CountryEntity, String> {
    CountryEntity findByCode(String code);
    //boolean existByCode(String code);
}
